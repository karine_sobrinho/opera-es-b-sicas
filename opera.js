<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Somando valores</title>
    <style>
        body{
            background-color: palevioletred;
            font: normal 20pt arial;
            text-align: center;
        }
        input{
            font: 15pt arial;
            width: 100px;
            color:darkred;
            background-color: black;
        }
        div#res{
            margin-top: 15px;
        }
    </style>
</head>
<body>
    <h1>Operações matemáticas básicas</h1>
    <h2>Somando valores</h2>
    <input type="number" name="n1" id="n1">
    <input type="number" name="n2" id="n2">
    <input type="button" value="somar" onclick="somar()">
    <div id="res"> Resultado</div>
    <script>
        function somar(){
            var tn1 = document.getElementById('n1')
            var tn2 = document.getElementById('n2')
            var res = document.getElementById('res')
            var nn1 = Number(tn1.value)
            var nn2 = Number(tn2.value)
            var s = nn1 + nn2
            res.innerHTML = `A soma de ${nn1} e ${nn2} é igual a <strong> ${s}</strong>`
            
        }
    </script>
    <h2>Subtraindo valores</h2>
    <input type="number" name="n1s" id="n1s">
    <input type="number" name="n2s" id="n2s">
    <input type="button" value="subtrair" onclick="subtrair()">
    <div id="rest"> Resultado</div>
    <script>
        function subtrair(){
            var tn1s = document.getElementById('n1s')
            var tn2s = document.getElementById('n2s')
            var res = document.getElementById('rest')
            var nn1s = Number(tn1s.value)
            var nn2s = Number(tn2s.value)
            var ss = nn1s - nn2s
            rest.innerHTML = `A subtração de ${nn1s} e ${nn2s} é igual a <strong> ${ss}</strong>`
            
        }
    </script>
    <h2>Multiplicando valores</h2>
    <input type="number" name="n1m" id="n1m">
    <input type="number" name="n2m" id="n2m">
    <input type="button" value="Multiplicar" onclick="multiplicae()">
    <div id="resul">Resultado</div>
    <script>
        function multiplicae(){
            var n1mm = document.getElementById('n1m')
            var n2mm = document.getElementById('n2m')
            var resul = document.getElementById('resul')
            var nn1m = Number(n1mm.value)
            var nn2m = Number(n2mm.value)
            var m = nn1m * nn2m
            resul.innerHTML = `A multiplicação de ${nn1m} e ${nn2m} é igual a <strong>${m}</strong>`

        }
    </script>
    <h2>Dividindo valores</h2>
    <input type="number" name="n1d" id="n1d">
    <input type="number" name="n2d" id="n2d">
    <input type="button" value="dividir" onclick="dividir()"> 
    <div id="result">Resultado</div>
    <script>
        function dividir(){
            var nn1d = document.getElementById('n1d')
            var nn2d = document.getElementById('n2d')
            var result = document.getElementById('result')
            var nn1dd = Number(nn1d.value)
            var nn2dd = Number(nn2d.value)
            var d = nn1dd / nn2dd 
            result.innerHTML = `A divisão de ${nn1dd} e ${nn2dd} é igual a <strong>${d}<strong>`
        }
    </script>
</body>
</html>